extern crate nix;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

use std::fs::File;
use std::fs::OpenOptions;
use nix::libc::{c_int, c_char, c_uint, c_long, c_ulong, uint32_t};
use nix::convert_ioctl_res;
use nix::request_code_none;
use nix::request_code_read;
use nix::request_code_write;
use nix::request_code_readwrite;
use nix::ioc;
use std::mem;
use std::rc::Rc; 
use std::ffi::CStr;
use std::ffi::CString;
//use nix::ioctl_readwrite;
use std::os::unix::io::RawFd;
use std::os::unix::io::AsRawFd;
use std::os::unix::io::IntoRawFd;

mod xioctl;

const DRM_DISPLAY_INFO_LEN: usize = 32;
const DRM_CONNECTOR_NAME_LEN: usize = 32;
const DRM_DISPLAY_MODE_LEN: usize = 32;
const DRM_PROP_NAME_LEN: usize = 32;

pub struct Version
{
    v_major: u32,
    v_minor: u32,
    v_patchlevel: u32,
    name: String,
    date: String,
    desc: String
}

impl Version
{
    fn new() -> Self
    {
        Version {
            v_major: 0,
            v_minor: 0,
            v_patchlevel: 0,
            name: "".to_string(),
            date: "".to_string(),
            desc: "".to_string()
        }
    }
    
    pub fn major(&self) -> u32
    {
        self.v_major
    }
    
    pub fn minor(&self) -> u32
    {
        self.v_minor
    }
    
    pub fn patchlevel(&self) -> u32
    {
        self.v_patchlevel
    }
    
    pub fn name(&self) -> &String
    {
        &self.name
    }
    
    pub fn date(&self) -> &String
    {
        &self.date
    }
    
    pub fn desc(&self) -> &String
    {
        &self.desc
    }
}

#[derive(Debug)]
pub enum drmModeConnection
{
    DRM_MODE_CONNECTED,
    DRM_MODE_DISCONNECTED,
    DRM_MODE_UNKNOWNCONNECTION
}

#[derive(Debug)]
pub enum drmModeSubPixel
{
    DRM_MODE_SUBPIXEL_UNKNOWN,
    DRM_MODE_SUBPIXEL_HORIZONTAL_RGB,
    DRM_MODE_SUBPIXEL_HORIZONTAL_BGR,
    DRM_MODE_SUBPIXEL_VERTICAL_RGB,
    DRM_MODE_SUBPIXEL_VERTICAL_BGR,
    DRM_MODE_SUBPIXEL_NONE
}

#[derive(Debug)]
pub enum DPMS_State
{
    ON,
    STANDBY,
    SUSPEND,
    OFF
}

#[derive(Debug)]
pub struct drmModeModeInfo
{
    pub clock: u32,
    pub hdisplay: u16,
    pub hsync_start: u16, 
    pub hsync_end: u16, 
    pub htotal: u16, 
    pub hskew: u16,
    pub vdisplay: u16, 
    pub vsync_start: u16,
    pub vsync_end: u16,
    pub vtotal: u16,
    pub vscan: u16,
    pub vrefresh: u32,
    pub flags: u32,
    pub type_: u32,
    pub name: String
}

#[derive(Debug)]
pub struct drmModePropertyEnum
{
    val: u64,
    name: String
}

#[derive(Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_version
{
    pub v_major: c_int,
    pub v_minor: c_int,
    pub v_patchlevel: c_int,
    pub name_len: usize,
    pub name: *mut c_char, /**< Name of driver */
    pub date_len: usize,
    pub date: *mut c_char, /**< User-space buffer to hold date */
    pub desc_len: usize,
    pub desc: *mut c_char
}

#[derive(Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_mode_res
{
    pub count_fbs: c_int,
    pub fbs: *mut uint32_t,
    pub count_crtcs: c_int,
    pub crtcs: *mut uint32_t,
    pub count_connectors: c_int,
    pub connectors: *mut uint32_t,
    pub count_encoders: c_int,
    pub encoders: *mut uint32_t,
    pub min_width: u32,
    pub max_width: u32,
    pub min_height: u32,
    pub max_height: u32,
}

#[derive(Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_mode_card_res
{
	pub fb_id_ptr: *mut u32,
	pub crtc_id_ptr: *mut u32,
	pub connector_id_ptr: *mut u32,
	pub encoder_id_ptr: *mut u32,
	pub count_fbs: u32,
	pub count_crtcs: u32,
	pub count_connectors: u32,
	pub count_encoders: u32,
    pub min_width: u32,
    pub max_width: u32,
    pub min_height: u32,
    pub max_height: u32,
}

#[derive(Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_mode_get_connector
{
    pub encoders_ptr: *mut u32,
    pub modes_ptr: *mut drm_mode_modeinfo,
    pub props_ptr: *mut u32,
    pub prop_values_ptr: *mut u64,
    pub count_modes: u32,
    pub count_props: u32,
    pub count_encoders: u32,
    pub encoder_id: u32, /**< Current Encoder */
    pub connector_id: u32, /**< Id */
    pub connector_type: u32,
    pub connector_type_id: u32,
    pub connection: u32,
    pub mm_width: u32,  /**< width in millimeters */
    pub mm_height: u32, /**< height in millimeters */
    pub subpixel: u32,
    pub pad: u32,
}

#[derive(Default, Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_mode_modeinfo
{
    clock: u32,
    hdisplay: u16,
    hsync_start: u16,
    hsync_end: u16,
    htotal: u16,
    hskew: u16,
    vdisplay: u16,
    vsync_start: u16,
    vsync_end: u16,
    vtotal: u16,
    vscan: u16,
    vrefresh: u32,
    flags: u32,
    type_: u32,
    name: [i8; DRM_DISPLAY_MODE_LEN],
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_auth
{
    pub magic: c_uint
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_get_cap
{
    pub capability: u64,
    pub value: u64,
}


pub mod drm_vblank_seq_type 
{
    pub type Type = u32;
    pub const _DRM_VBLANK_ABSOLUTE: Type = 0;
    pub const _DRM_VBLANK_RELATIVE: Type = 1;
    pub const _DRM_VBLANK_HIGH_CRTC_MASK: Type = 62;
    pub const _DRM_VBLANK_EVENT: Type = 67108864;
    pub const _DRM_VBLANK_FLIP: Type = 134217728;
    pub const _DRM_VBLANK_NEXTONMISS: Type = 268435456;
    pub const _DRM_VBLANK_SECONDARY: Type = 536870912;
    pub const _DRM_VBLANK_SIGNAL: Type = 1073741824;
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_wait_vblank_request
{
    pub type_: drm_vblank_seq_type::Type,
    pub sequence: c_uint,
    pub signal: c_ulong,
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_wait_vblank_reply
{
    pub type_: drm_vblank_seq_type::Type,
    pub sequence: c_uint,
    pub tval_sec: c_long,
    pub tval_usec: c_long,
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_mode_property_enum
{
	value: u64,
	name: [i8; DRM_PROP_NAME_LEN],
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_mode_get_property
{
    values_ptr: *mut u64, /* values and blob lengths */
    // TODO: remake to *mut c_void and add logic below
    enum_blob_ptr: *mut drm_mode_property_enum, /* enum and blob id ptrs */
    prop_id: u32,
    flags: u32,
    name: [i8; DRM_PROP_NAME_LEN],
    count_values: u32,
    //  /* This is only used to count enum values, not blobs. The _blobs is
    //  * simply because of a historical reason, i.e. backwards compat. */
    count_enum_blobs: u32,
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_mode_connector_set_property
{
    pub value: u64,
    pub prop_id: u32,
    pub connector_id: u32
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_mode_crtc_lut {
    pub crtc_id: u32,
    pub gamma_size: u32,
    /* pointers to arrays */
    pub red: u64,
    pub green: u64,
    pub blue: u64
}

const DRM_CAP_DUMB_BUFFER: u32 = 1;
const DRM_CAP_VBLANK_HIGH_CRTC: u32 = 2;
const DRM_CAP_DUMB_PREFERRED_DEPTH: u32 = 3;
const DRM_CAP_DUMB_PREFER_SHADOW: u32 = 4;
const DRM_CAP_PRIME: u32 = 5;
const DRM_CAP_TIMESTAMP_MONOTONIC: u32 = 6;
const DRM_CAP_ASYNC_PAGE_FLIP: u32 = 7;
const DRM_CAP_CURSOR_WIDTH: u32 = 8;
const DRM_CAP_CURSOR_HEIGHT: u32 = 9;
const DRM_CAP_ADDFB2_MODIFIERS: u32 = 16;
const DRM_CAP_PAGE_FLIP_TARGET: u32 = 17;
const DRM_CAP_CRTC_IN_VBLANK_EVENT: u32 = 18;
const DRM_CAP_SYNCOBJ: u32 = 19;
//
const DRM_MODE_ENCODER_NONE: u32 = 0;
const DRM_MODE_ENCODER_DAC: u32 = 1;
const DRM_MODE_ENCODER_TMDS: u32 = 2;
const DRM_MODE_ENCODER_LVDS: u32 = 3;
const DRM_MODE_ENCODER_TVDAC: u32 = 4;
const DRM_MODE_ENCODER_VIRTUAL: u32 = 5;
const DRM_MODE_ENCODER_DSI: u32 = 6;
const DRM_MODE_ENCODER_DPMST: u32 = 7;
const DRM_MODE_ENCODER_DPI: u32 = 8;
//
const DRM_MODE_CONNECTOR_Unknown: u32 = 0;
const DRM_MODE_CONNECTOR_VGA: u32 = 1;
const DRM_MODE_CONNECTOR_DVII: u32 = 2;
const DRM_MODE_CONNECTOR_DVID: u32 = 3;
const DRM_MODE_CONNECTOR_DVIA: u32 = 4;
const DRM_MODE_CONNECTOR_Composite: u32 = 5;
const DRM_MODE_CONNECTOR_SVIDEO: u32 = 6;
const DRM_MODE_CONNECTOR_LVDS: u32 = 7;
const DRM_MODE_CONNECTOR_Component: u32 = 8;
const DRM_MODE_CONNECTOR_9PinDIN: u32 = 9;
const DRM_MODE_CONNECTOR_DisplayPort: u32 = 10;
const DRM_MODE_CONNECTOR_HDMIA: u32 = 11;
const DRM_MODE_CONNECTOR_HDMIB: u32 = 12;
const DRM_MODE_CONNECTOR_TV: u32 = 13;
const DRM_MODE_CONNECTOR_eDP: u32 = 14;
const DRM_MODE_CONNECTOR_VIRTUAL: u32 = 15;
const DRM_MODE_CONNECTOR_DSI: u32 = 16;
const DRM_MODE_CONNECTOR_DPI: u32 = 17;
const DRM_MODE_CONNECTOR_WRITEBACK: u32 = 18;
//
const DRM_MODE_PROP_PENDING	: u32 = 0x01; /* deprecated, do not use */
const DRM_MODE_PROP_RANGE: u32 = 0x02;
const DRM_MODE_PROP_IMMUTABLE: u32 = 0x04;
const DRM_MODE_PROP_ENUM: u32 = 0x08;  /* enumerated type with text strings */
const DRM_MODE_PROP_BLOB: u32 = 0x10;
const DRM_MODE_PROP_BITMASK: u32 = 0x20;  /* bitmask of enumerated types */
//
pub const DRM_MODE_DPMS_ON: u8 = 0;
pub const DRM_MODE_DPMS_STANDBY: u8 = 1;
pub const DRM_MODE_DPMS_SUSPEND: u8 = 2;
pub const DRM_MODE_DPMS_OFF: u8 = 3;
//
const DRM_IOCTL_BASE: u8 = b'd';

xioctl_readwrite!(drmGetVersion, DRM_IOCTL_BASE, 0x00, drm_version);
xioctl_none!(drmSetMaster, DRM_IOCTL_BASE, 0x1E);
xioctl_none!(drmDropMaster, DRM_IOCTL_BASE, 0x1F);
xioctl_read!(drmGetMagic, DRM_IOCTL_BASE, 0x02, drm_auth);
xioctl_write_ptr!(drmAuthMagic, DRM_IOCTL_BASE, 0x11, drm_auth);
xioctl_readwrite!(drmGetCap, DRM_IOCTL_BASE, 0x0c, drm_get_cap);
//xioctl_readwrite!(drmWaitVBlank, DRM_IOCTL_BASE, 0x3a, drm_wait_vblank);
xioctl_readwrite!(drmModeGetResources, DRM_IOCTL_BASE, 0xA0, drm_mode_card_res);
xioctl_readwrite!(drmModeGetCrtc, DRM_IOCTL_BASE, 0xA1, drm_mode_card_res);
xioctl_readwrite!(drmModeSetCrtc, DRM_IOCTL_BASE, 0xA2, drm_mode_card_res);
xioctl_readwrite!(drmModeCrtcGetGamma, DRM_IOCTL_BASE, 0xA4, drm_mode_crtc_lut);
xioctl_readwrite!(drmModeCrtcSetGamma, DRM_IOCTL_BASE, 0xA5, drm_mode_crtc_lut);
xioctl_readwrite!(drmModeGetEncoder, DRM_IOCTL_BASE, 0xA6, drm_mode_card_res);
xioctl_readwrite!(drmModeGetConnector, DRM_IOCTL_BASE, 0xA7, drm_mode_get_connector);
xioctl_readwrite!(drmModeGetProperty, DRM_IOCTL_BASE, 0xAA, drm_mode_get_property);
//xioctl_readwrite!(drmModeConnectorGetProperty, DRM_IOCTL_BASE, 0xAA, drm_mode_get_property);
xioctl_readwrite!(drmModeConnectorSetProperty, DRM_IOCTL_BASE, 0xAB, drm_mode_connector_set_property);

// __--====================================================--__
// __--====================================================--__
// __--====================================================--__

pub fn char_ptr_to_string(p: *const i8) -> String
{
    let res_str = unsafe {
        let nm = mem::transmute::<*const i8, *mut i8>(p);
        CStr::from_ptr(nm).to_str().unwrap().to_string().clone()
    };
    res_str
}

// .........
// .........
// .........

#[derive(Debug)]
pub struct Prop
{
    prop_id: u32,
    name: String,
    flags: u32,
    values: Vec<drmModePropertyEnum>
}

impl Prop
{
    pub fn name(&self) -> String
    {
        self.name.clone()
    }
    
    pub fn id(&self) -> u32
    {
        self.prop_id
    }
}

pub struct Properies<'a>
{
    //dev: Rc<Dev>,
    dev: &'a Dev,
    it: std::slice::Iter<'a, u32>
}

impl<'a> Iterator for Properies<'a>
{
    type Item = Prop;
    
    fn next(&mut self) -> Option<Self::Item>
    {
        if let Some(&id) = self.it.next() {
            let mut p: drm_mode_get_property = unsafe { std::mem::zeroed() };
            p.prop_id = id;
            
            unsafe {  drmModeGetProperty(self.dev.as_raw_fd(), &mut p); }

            let mut values: Vec<u64> = Vec::with_capacity(p.count_values as usize);
            let mut enum_blobs: Vec<drm_mode_property_enum> = 
                Vec::with_capacity(p.count_enum_blobs as usize);

            if p.count_enum_blobs > 0 {
                if (p.flags & DRM_MODE_PROP_BLOB) > 0 {
                    // TODO:
                    // let mut values: Vec<u32> = Vec::with_capacity(p.count_enum_blobs as usize);
                    // let mut enum_blobs: Vec<u32> = Vec::with_capacity(p.count_enum_blobs as usize);
                    // p.values_ptr = values.as_mut_ptr();
                    // p.enum_blob_ptr = enum_blobs.as_mut_ptr();
                    enum_blobs.resize(p.count_enum_blobs as usize, Default::default());
                    p.enum_blob_ptr = enum_blobs.as_mut_ptr();
                }
                else if (p.flags & (DRM_MODE_PROP_ENUM | DRM_MODE_PROP_BITMASK)) > 0 {
                    enum_blobs.resize(p.count_enum_blobs as usize, Default::default());
                    p.enum_blob_ptr = enum_blobs.as_mut_ptr();
                }
            }
            if p.count_values > 0 {
                values.resize(p.count_values as usize, 0);
                p.values_ptr = values.as_mut_ptr();
            }

            unsafe {  drmModeGetProperty(self.dev.as_raw_fd(), &mut p); }

            let mut vals: Vec<drmModePropertyEnum>;
            if (p.flags & (DRM_MODE_PROP_ENUM | DRM_MODE_PROP_BITMASK)) > 0 {
                vals = Vec::with_capacity(p.count_enum_blobs as usize);
                for b in enum_blobs {
                    vals.push(
                        drmModePropertyEnum {
                            val: b.value,
                            name: char_ptr_to_string((&b.name).as_ptr())
                        }
                    );
                }
            }
            else if (p.flags & DRM_MODE_PROP_BLOB) > 0 {
                // TODO:
                // vals = p.values_ptr;
                // blob_ids = p.enum_blob_ptr;
                vals = Vec::with_capacity(0);
            }
            else {
                vals = Vec::with_capacity(0);
            }

            let res = Prop {
                prop_id: p.prop_id,
                flags: p.flags,
                name: char_ptr_to_string((&p.name).as_ptr()),
                values: vals
            };

            Some(res)
        }
        else {
            None
        }
    }
}

#[derive(Debug)]
pub struct Conn<'a>
{
    //dev: Rc<Dev>,
    dev: &'a Dev,
    connector_id: u32, /**< Id */
    encoder_id: u32, /**< Encoder currently connected to */
    connector_type: u32,
    connector_type_id: u32,
    connection: drmModeConnection,
    mmWidth: u32,
    mmHeight: u32, /**< HxW in millimeters */
    subpixel: drmModeSubPixel,
    modes: Vec<drmModeModeInfo>,
    props: Vec<u32>,  /**< List of property ids */
    //prop_values: Vec<u64>,  /**< List of property values */
    encoders: Vec<u32>  // < List of encoder ids
}

impl Conn<'_>
{
    pub fn id(&self) -> u32
    {
        self.connector_id
    }
    
    pub fn is_connected(&self) -> bool
    {
        match self.connection {
            drmModeConnection::DRM_MODE_CONNECTED => true,
            _ => false
        }
    }
    
    pub fn properties(&self) -> Properies
    {
        Properies {
            dev: self.dev.clone(),
            it: self.props.iter()
        }
    }
}

pub struct Connectors<'a>
{
    //dev: Rc<Dev>,
    dev: &'a Dev,
    it: std::slice::Iter<'a, u32>
}

impl<'a> Iterator for Connectors<'a>
{
    type Item = Conn<'a>;
    
    fn next(&mut self) -> Option<Self::Item>
    {
        if let Some(&id) = self.it.next() {
            let mut c: drm_mode_get_connector = 
                unsafe { std::mem::zeroed() };

            c.connector_id = id;

            unsafe {  drmModeGetConnector(self.dev.as_raw_fd(), &mut c); }

            let mut encoders: Vec<u32> = Vec::with_capacity(c.count_encoders as usize);
            if c.count_encoders > 0 {
                encoders.resize(c.count_encoders as usize, 0);
                c.encoders_ptr = encoders.as_mut_ptr();
            }

            let mut modes_in: Vec<drm_mode_modeinfo> = Vec::with_capacity(c.count_modes as usize);
            if c.count_modes > 0 {
                modes_in.resize(c.count_modes as usize, Default::default());
                c.modes_ptr = modes_in.as_mut_ptr();
            }
            
            let mut props: Vec<u32> = Vec::with_capacity(c.count_props as usize);
            let mut prop_values: Vec<u64> = Vec::with_capacity(c.count_props as usize);
            if c.count_props > 0 {
                props.resize(c.count_props as usize, 0);
                c.props_ptr = props.as_mut_ptr();
                prop_values.resize(c.count_props as usize, 0);
                c.prop_values_ptr = prop_values.as_mut_ptr();
            }

            unsafe {  drmModeGetConnector(self.dev.as_raw_fd(), &mut c); }

            let connection = match c.connection {
                1 => drmModeConnection::DRM_MODE_CONNECTED,
                2 => drmModeConnection::DRM_MODE_DISCONNECTED,
                3 => drmModeConnection::DRM_MODE_UNKNOWNCONNECTION,
                _ => drmModeConnection::DRM_MODE_UNKNOWNCONNECTION
            };
            
            let subpixel = match c.subpixel {
                0 => drmModeSubPixel::DRM_MODE_SUBPIXEL_UNKNOWN,
                1 => drmModeSubPixel::DRM_MODE_SUBPIXEL_HORIZONTAL_RGB,
                2 => drmModeSubPixel::DRM_MODE_SUBPIXEL_HORIZONTAL_BGR,
                3 => drmModeSubPixel::DRM_MODE_SUBPIXEL_VERTICAL_RGB,
                4 => drmModeSubPixel::DRM_MODE_SUBPIXEL_VERTICAL_BGR,
                5 => drmModeSubPixel::DRM_MODE_SUBPIXEL_NONE,
                _ => drmModeSubPixel::DRM_MODE_SUBPIXEL_UNKNOWN
            };
            
            let mut modes_out: Vec<drmModeModeInfo> = Vec::with_capacity(modes_in.len());
            for m in modes_in {
                modes_out.push(drmModeModeInfo {
                    clock: m.clock,
                    hdisplay: m.hdisplay,
                    hsync_start: m.hsync_start, 
                    hsync_end: m.hsync_end, 
                    htotal: m.htotal, 
                    hskew: m.hskew,
                    vdisplay: m.vdisplay, 
                    vsync_start: m.vsync_start,
                    vsync_end: m.vsync_end,
                    vtotal: m.vtotal,
                    vscan: m.vscan,
                    vrefresh: m.vrefresh,
                    flags: m.flags,
                    type_: m.type_,
                    name: char_ptr_to_string((&m.name).as_ptr()),
                    }
                );
            }
            
            Some( Conn {
                dev: self.dev.clone(),
                connector_id: c.connector_id,
                encoder_id: c.encoder_id,
                connector_type: c.connector_type,
                connector_type_id: c.connector_type_id,
                connection: connection,
                mmWidth: c.mm_width,
                mmHeight: c.mm_height,
                subpixel: subpixel,
                modes: modes_out,
                props: props,
                //prop_values: prop_values,
                encoders: encoders,
            })
        }
        else {
            None
        }
    }
}

pub struct Resources<'a>
{
    //dev: Rc<Dev>,
    dev: &'a Dev,
    fbs_ids: Vec<u32>,
    crtc_ids: Vec<u32>,
    enc_ids: Vec<u32>,
    conn_ids: Vec<u32>
}

impl Resources<'_>
{
    pub fn fbs(&self)
    {
    }
    
    pub fn crtcs(&self)
    {
    }
    
    pub fn encoders(&self)
    {
    }
    
    pub fn connectors(&self) -> Connectors
    {
        Connectors {
            dev: self.dev.clone(),
            it: self.conn_ids.iter()
        }
    }
}



pub struct Dev
{
    f: std::fs::File
}

impl Dev
{
    pub fn open(path: &str) -> Self
    {
        let mut options = OpenOptions::new();
        options.read(true);
        options.write(true);
        Self::new(options.open(path).unwrap())
    }

    pub fn new(file: std::fs::File) -> Self
    {
        Self {f: file}
    }
    
    pub fn into_file(self) -> std::fs::File
    {
        self.f
    }
    
    pub fn get_version(&self) -> Version
    {
        let mut ver: drm_version = unsafe { std::mem::zeroed() };
        let fd = self.as_raw_fd();

        unsafe { drmGetVersion(fd, &mut ver); }
        let name: String;
        let date: String;
        let desc: String;
       
        unsafe { 
            if ver.name_len > 0 {
                ver.name = CString::from_vec_unchecked(vec![0; ver.name_len + 1]).into_raw();
            }
            if ver.date_len > 0 {
                ver.date = CString::from_vec_unchecked(vec![0; ver.date_len + 1]).into_raw();
            }
            if ver.desc_len > 0 {
                ver.desc = CString::from_vec_unchecked(vec![0; ver.desc_len + 1]).into_raw();
            }
            drmGetVersion(fd, &mut ver);
            name = if !ver.name.is_null() {
                CString::from_raw(ver.name).into_string().unwrap().clone()
            }
            else {
                String::new()
            };
            date = if !ver.date.is_null() {
                CString::from_raw(ver.date).into_string().unwrap().clone()
            }
            else {
                String::new()
            };
            desc = if !ver.desc.is_null() {
                CString::from_raw(ver.desc).into_string().unwrap().clone()
            }
            else {
                String::new()
            };
        }
        
        Version {
            v_major: ver.v_major as u32,
            v_minor: ver.v_minor as u32,
            v_patchlevel: ver.v_patchlevel as u32,
            name: name,
            date: date,
            desc: desc
        }
    }
    
    pub fn acquire_master(&self)
    {
        let res = unsafe { drmSetMaster(self.as_raw_fd()) };
        if let Err(err) = res {
        }
    }
    
    pub fn release_master(&self)
    {
        let res = unsafe { drmDropMaster(self.as_raw_fd()) };
        if let Err(err) = res {
        }
    }
    
    pub fn get_token(&self) -> u32
    {
        let mut auth: drm_auth = unsafe { std::mem::zeroed() };
        unsafe {  drmGetMagic(self.as_raw_fd(), &mut auth); }
        auth.magic
    }
    
    pub fn auth_token(&self, mgc: u32)
    {
        let auth = drm_auth { magic: mgc};
        unsafe { drmAuthMagic(self.as_raw_fd(), &auth); }
    }
    
    pub fn capDumbBuffer(&self) -> bool
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_DUMB_BUFFER.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        if get_cap.value == 1 {
            return true;
        }
        return false;
    }
    
    pub fn capVBlankHighCRTC(&self) -> bool
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_VBLANK_HIGH_CRTC.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        if get_cap.value == 1 {
            return true;
        }
        return false;
    }
    
    pub fn capTimestampMonotonic(&self) -> bool
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_TIMESTAMP_MONOTONIC.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        if get_cap.value == 1 {
            return true;
        }
        return false;
    }
    
    pub fn capCursorWidth(&self) -> u64
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_CURSOR_WIDTH.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        return get_cap.value;
    }
    
    pub fn capCursorHeight(&self) -> u64
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_CURSOR_HEIGHT.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        return get_cap.value;
    }
    
    pub fn waitVBlank()
    {
    }
    
    pub fn resources(&self) -> Resources
    {
        let mut r: drm_mode_card_res = unsafe { std::mem::zeroed() };
        
        unsafe {  drmModeGetResources(self.as_raw_fd(), &mut r); }
        
        let mut fbs: Vec<u32> = Vec::with_capacity(r.count_fbs as usize);
        if r.count_fbs > 0 {
            fbs.resize(r.count_fbs as usize, 0);
            r.fb_id_ptr = fbs.as_mut_ptr();
        }
        let mut crtcs: Vec<u32> = Vec::with_capacity(r.count_crtcs as usize);
        if r.count_crtcs > 0 {
            crtcs.resize(r.count_crtcs as usize, 0);
            r.crtc_id_ptr = crtcs.as_mut_ptr();
        }
        let mut encs: Vec<u32> = Vec::with_capacity(r.count_encoders as usize);
        if r.count_encoders > 0 {
            encs.resize(r.count_encoders as usize, 0);
            r.encoder_id_ptr = encs.as_mut_ptr();
        }
        let mut conns: Vec<u32> = Vec::with_capacity(r.count_connectors as usize);
        if r.count_connectors > 0 {
            conns.resize(r.count_connectors as usize, 0);
            r.connector_id_ptr = conns.as_mut_ptr();
        }
        
        unsafe {  drmModeGetResources(self.as_raw_fd(), &mut r); }

        Resources {
            //dev: Rc::new(self),
            dev: self,
            fbs_ids: fbs,
            crtc_ids: crtcs,
            enc_ids: encs,
            conn_ids: conns
        }
    }
}

impl AsRawFd for Dev
{
    fn as_raw_fd(&self) -> RawFd
    {
        self.f.as_raw_fd()
    }
}

impl std::fmt::Debug for Dev
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result
    {
        write!(f, "Dev")
    }
}

pub struct Connector<'a>
{
    dev: &'a Dev,
    id: u32,
}

impl Connector<'_>
{
    pub fn set_property(&self, prop_id: u32, val: u64)
    {
        let mut set_property: drm_mode_connector_set_property = 
            unsafe { std::mem::zeroed() };
        set_property.connector_id = self.id;
        set_property.prop_id = prop_id;
        set_property.value = val;
        unsafe { drmModeConnectorSetProperty(
            self.dev.as_raw_fd(), &mut set_property); }
    }
}

pub struct Control<'a>
{
    dev: &'a Dev
}

impl<'a> Control<'a>
{
    pub fn new(d: &'a Dev) -> Self
    {
        Self {
            dev: d
        }
    }
    
    pub fn get_connector(&self, id: u32) -> Connector
    {
        Connector {
            dev: self.dev,
            id: id
        }
    }
}
