//extern crate libc;
#[macro_use]
extern crate nix;

use std::fs::File;
use std::fs::OpenOptions;
use nix::libc::{c_int, c_char, c_uint, c_long, c_ulong, uint32_t};
use std::ffi::CString;
//use nix::ioctl_readwrite;
use std::os::unix::io::RawFd;
use std::os::unix::io::AsRawFd;
use std::os::unix::io::IntoRawFd;

#[macro_use]
mod xioctl;

pub struct Version
{
    pub v_major: u32,
    pub v_minor: u32,
    pub v_patchlevel: u32,
    pub name: String,
    pub date: String,
    pub desc: String
}

impl Version
{
    fn new() -> Self
    {
        Version {
            v_major: 0,
            v_minor: 0,
            v_patchlevel: 0,
            name: "".to_string(),
            date: "".to_string(),
            desc: "".to_string()
        }
    }
}

#[derive(Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_version
{
    pub v_major: c_int,
    pub v_minor: c_int,
    pub v_patchlevel: c_int,
    pub name_len: usize,
    pub name: *mut c_char, /**< Name of driver */
    pub date_len: usize,
    pub date: *mut c_char, /**< User-space buffer to hold date */
    pub desc_len: usize,
    pub desc: *mut c_char
}

#[derive(Clone, Copy, PartialEq)]
#[repr(C)]
pub struct drm_mode_res
{
    pub count_fbs: c_int,
    pub fbs: *mut uint32_t,
    pub count_crtcs: c_int,
    pub crtcs: *mut uint32_t,
    pub count_connectors: c_int,
    pub connectors: *mut uint32_t,
    pub count_encoders: c_int,
    pub encoders: *mut uint32_t,
    pub min_width: u32,
    pub max_width: u32,
    pub min_height: u32,
    pub max_height: u32,
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_auth
{
    pub magic: c_uint
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_get_cap
{
    pub capability: u64,
    pub value: u64,
}


pub mod drm_vblank_seq_type 
{
    pub type Type = u32;
    pub const _DRM_VBLANK_ABSOLUTE: Type = 0;
    pub const _DRM_VBLANK_RELATIVE: Type = 1;
    pub const _DRM_VBLANK_HIGH_CRTC_MASK: Type = 62;
    pub const _DRM_VBLANK_EVENT: Type = 67108864;
    pub const _DRM_VBLANK_FLIP: Type = 134217728;
    pub const _DRM_VBLANK_NEXTONMISS: Type = 268435456;
    pub const _DRM_VBLANK_SECONDARY: Type = 536870912;
    pub const _DRM_VBLANK_SIGNAL: Type = 1073741824;
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_wait_vblank_request
{
    pub type_: drm_vblank_seq_type::Type,
    pub sequence: c_uint,
    pub signal: c_ulong,
}

#[repr(C)]
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_wait_vblank_reply
{
    pub type_: drm_vblank_seq_type::Type,
    pub sequence: c_uint,
    pub tval_sec: c_long,
    pub tval_usec: c_long,
}

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct drm_mode_connector_set_property
{
    pub value: u64,
    pub prop_id: u32,
    pub connector_id: u32
}

const DRM_CAP_DUMB_BUFFER: u32 = 1;
const DRM_CAP_VBLANK_HIGH_CRTC: u32 = 2;
const DRM_CAP_DUMB_PREFERRED_DEPTH: u32 = 3;
const DRM_CAP_DUMB_PREFER_SHADOW: u32 = 4;
const DRM_CAP_PRIME: u32 = 5;
const DRM_CAP_TIMESTAMP_MONOTONIC: u32 = 6;
const DRM_CAP_ASYNC_PAGE_FLIP: u32 = 7;
const DRM_CAP_CURSOR_WIDTH: u32 = 8;
const DRM_CAP_CURSOR_HEIGHT: u32 = 9;
const DRM_CAP_ADDFB2_MODIFIERS: u32 = 16;
const DRM_CAP_PAGE_FLIP_TARGET: u32 = 17;
const DRM_CAP_CRTC_IN_VBLANK_EVENT: u32 = 18;
const DRM_CAP_SYNCOBJ: u32 = 19;
//
const DRM_MODE_DPMS_ON: u8 = 0;
const DRM_MODE_DPMS_STANDBY: u8 = 1;
const DRM_MODE_DPMS_SUSPEND: u8 = 2;
const DRM_MODE_DPMS_OFF: u8 = 3;


const DRM_IOCTL_BASE: u8 = b'd';

xioctl_readwrite!(drmGetVersion, DRM_IOCTL_BASE, 0x00, drm_version);
xioctl_none!(drmSetMaster, DRM_IOCTL_BASE, 0x1E);
xioctl_none!(drmDropMaster, DRM_IOCTL_BASE, 0x1F);
xioctl_read!(drmGetMagic, DRM_IOCTL_BASE, 0x02, drm_auth);
xioctl_write_ptr!(drmAuthMagic, DRM_IOCTL_BASE, 0x11, drm_auth);
xioctl_readwrite!(drmGetCap, DRM_IOCTL_BASE, 0x0c, drm_get_cap);
//xioctl_readwrite!(drmWaitVBlank, DRM_IOCTL_BASE, 0x3a, drm_wait_vblank);
xioctl_readwrite!(drmModeConnectorSetProperty, DRM_IOCTL_BASE, 0xAB, drm_mode_connector_set_property);

pub struct Conn
{
}

pub struct Connectors
{
}

impl Iterator for Connectors 
{
    type Item = Conn;
    
    fn next(&mut self) -> Option<Conn>
    {
        None
    }
}

pub struct Dev
{
    f: std::fs::File
}

impl Dev
{
    pub fn open(path: &str) -> Self
    {
        let mut options = OpenOptions::new();
        options.read(true);
        options.write(true);
        Self::new(options.open(path).unwrap())
    }

    pub fn new(file: std::fs::File) -> Self
    {
        Self {f: file}
    }
    
    pub fn into_file(self) -> std::fs::File
    {
        self.f
    }
    
    pub fn get_version(&self) -> Version
    {
        let mut ver: drm_version = unsafe { std::mem::zeroed() };
        //let mut ver: drm_version = unsafe { std::mem::uninitialized() };
        let fd = self.as_raw_fd();

        unsafe { drmGetVersion(fd, &mut ver); }
        let name: String;
        let date: String;
        let desc: String;
       
        unsafe { 
            if ver.name_len > 0 {
                ver.name = CString::from_vec_unchecked(vec![0; ver.name_len + 1]).into_raw();
            }
            if ver.date_len > 0 {
                ver.date = CString::from_vec_unchecked(vec![0; ver.date_len + 1]).into_raw();
            }
            if ver.desc_len > 0 {
                ver.desc = CString::from_vec_unchecked(vec![0; ver.desc_len + 1]).into_raw();
            }
            drmGetVersion(fd, &mut ver);
            name = if !ver.name.is_null() {
                CString::from_raw(ver.name).into_string().unwrap().clone()
            }
            else {
                String::new()
            };
            date = if !ver.date.is_null() {
                CString::from_raw(ver.date).into_string().unwrap().clone()
            }
            else {
                String::new()
            };
            desc = if !ver.desc.is_null() {
                CString::from_raw(ver.desc).into_string().unwrap().clone()
            }
            else {
                String::new()
            };
        }
        
        Version {
            v_major: ver.v_major as u32,
            v_minor: ver.v_minor as u32,
            v_patchlevel: ver.v_patchlevel as u32,
            name: name,
            date: date,
            desc: desc
        }
    }
    
    pub fn acquire_master(&self)
    {
        unsafe { drmSetMaster(self.as_raw_fd()) }
    }
    
    pub fn release_master(&self)
    {
        unsafe { drmDropMaster(self.as_raw_fd()); }
    }
    
    pub fn get_token(&self) -> u32
    {
        let mut auth: drm_auth = unsafe { std::mem::zeroed() };
        unsafe {  drmGetMagic(self.as_raw_fd(), &mut auth); }
        auth.magic
    }
    
    pub fn auth_token(&self, mgc: u32)
    {
        let auth = drm_auth { magic: mgc};
        unsafe { drmAuthMagic(self.as_raw_fd(), &auth); }
    }
    
    pub fn capDumbBuffer(&self) -> bool
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_DUMB_BUFFER.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        if get_cap.value == 1 {
            return true;
        }
        return false;
    }
    
    pub fn capVBlankHighCRTC(&self) -> bool
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_VBLANK_HIGH_CRTC.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        if get_cap.value == 1 {
            return true;
        }
        return false;
    }
    
    pub fn capTimestampMonotonic(&self) -> bool
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_TIMESTAMP_MONOTONIC.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        if get_cap.value == 1 {
            return true;
        }
        return false;
    }
    
    pub fn capCursorWidth(&self) -> u64
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_CURSOR_WIDTH.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        return get_cap.value;
    }
    
    pub fn capCursorHeight(&self) -> u64
    {
        let mut get_cap: drm_get_cap = unsafe { std::mem::zeroed() };
        get_cap.capability = DRM_CAP_CURSOR_HEIGHT.into();
        unsafe {  drmGetCap(self.as_raw_fd(), &mut get_cap); }
        return get_cap.value;
    }
    
    pub fn waitVBlank()
    {
    }
    
    pub fn fbs(&self)
    {
    }
    
    pub fn crtcs(&self)
    {
    }
    
    pub fn encoders(&self)
    {
    }
    
    pub fn connectors(&self) -> Connectors
    {
        Connectors {
        }
    }
}

impl AsRawFd for Dev
{
    fn as_raw_fd(&self) -> RawFd
    {
        self.f.as_raw_fd()
    }
}

/*pub unsafe fn drmGetVersion(fd: ::libc::c_int, data: *mut drm_version)
{
    use ::libc::{c_int, c_ulong, ioctl};
    const NRBITS: c_ulong = 8;
    const TYPEBITS: c_ulong = 8;

    // for x86/x86_64
    const NONE: c_ulong = 0;
    const READ: c_ulong = 2;
    const WRITE: c_ulong = 1;
    const SIZEBITS: c_ulong = 14;
    const DIRBITS: c_ulong = 2;
    
    const NRSHIFT: c_ulong = 0;
    const TYPESHIFT: c_ulong = NRSHIFT + NRBITS as c_ulong;
    const SIZESHIFT: c_ulong = TYPESHIFT + TYPEBITS as c_ulong;
    const DIRSHIFT: c_ulong = SIZESHIFT + SIZEBITS as c_ulong;
    
    const NRMASK: c_ulong = (1 << NRBITS) - 1;
    const TYPEMASK: c_ulong = (1 << TYPEBITS) - 1;
    const SIZEMASK: c_ulong = (1 << SIZEBITS) - 1;
    const DIRMASK: c_ulong = (1 << DIRBITS) - 1;
    
    const DIR: c_ulong = ((READ | WRITE) as c_ulong & DIRMASK) << DIRSHIFT;
    const TYPE: c_ulong = (DRM_IOCTL_BASE as c_ulong & TYPEMASK) << TYPESHIFT;
    const NR: c_ulong = (DRM_IOCTL_VERSION as c_ulong & NRMASK) << NRSHIFT;
    const SIZE: c_ulong = (::std::mem::size_of::<drm_version>() as c_ulong & SIZEMASK) << SIZESHIFT;
    
    let ret: c_int = ioctl(fd, DIR | TYPE | NR | SIZE, data);
}*/

fn set_property(fd: RawFd, connector_id: u32, property_id: u32, value: u64)
{
    let mut osp = drm_mode_connector_set_property {
        value: value,
        prop_id: property_id,
        connector_id: connector_id
    };
    unsafe { drmModeConnectorSetProperty(fd, &mut osp); }
}

fn main()
{
    let d = Dev::open("/dev/dri/card0");
    let v = d.get_version();
    println!("Version {}.{}.{}", v.v_major, v.v_minor, v.v_patchlevel);
    println!("Name: {:?}", v.name);
    println!("Date: {:?}", v.date);
    println!("Desc: {:?}", v.desc);
    println!("token: {:?}", d.get_token());
    println!("DRM_CAP_TIMESTAMP_MONOTONIC:  {:?}", d.capTimestampMonotonic());
    println!("DRM_CAP_CURSOR_WIDTH:  {:?}", d.capCursorWidth());
    println!("DRM_CAP_CURSOR_HEIGHT:  {:?}", d.capCursorHeight());
    //set_property(d.as_raw_fd(), 51, 2, DRM_MODE_DPMS_STANDBY as u64);
    //loop {
    //}
}
