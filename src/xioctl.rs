use nix::convert_ioctl_res;
//use nix::sys::ioctl;

#[macro_export]
macro_rules! xioctl_none {
    ($(#[$attr:meta])* $name:ident, $ioty:expr, $nr:expr) => (
        $(#[$attr])*
        pub unsafe fn $name(fd: nix::libc::c_int)
            -> nix::Result<nix::libc::c_int> 
        {
            loop {
                let ret = convert_ioctl_res!(
                    nix::libc::ioctl(
                        fd, 
                        request_code_none!(
                            $ioty, $nr
                        ) as nix::sys::ioctl::ioctl_num_type
                    )
                );
                if let Err(err) = ret {
                    if err == nix::Error::Sys(nix::errno::Errno::EINTR) || 
                        err == nix::Error::Sys(nix::errno::Errno::EAGAIN) {
                        continue;
                    }
                }
                return ret;
            }
        }
    )
}

#[macro_export]
macro_rules! xioctl_read {
    ($(#[$attr:meta])* $name:ident, $ioty:expr, $nr:expr, $ty:ty) => (
        $(#[$attr])*
        pub unsafe fn $name(fd: nix::libc::c_int, data: *mut $ty) -> 
            nix::Result<nix::libc::c_int>
        {
            loop {
                let ret = convert_ioctl_res!(
                    nix::libc::ioctl(
                        fd,
                        request_code_read!(
                            $ioty, $nr, 
                            ::std::mem::size_of::<$ty>()
                        ) as nix::sys::ioctl::ioctl_num_type,
                        data
                    )
                );
                if let Err(err) = ret {
                    if err == nix::Error::Sys(nix::errno::Errno::EINTR) || 
                        err == nix::Error::Sys(nix::errno::Errno::EAGAIN) {
                        continue;
                    }
                }
                return ret;
            }
        }
    )
}

#[macro_export]
macro_rules! xioctl_readwrite
{
    ($(#[$attr:meta])* $name:ident, $ioty:expr, $nr:expr, $ty:ty) => (
        $(#[$attr])*
        pub unsafe fn $name(fd: nix::libc::c_int, data: *mut $ty) ->
            nix::Result<nix::libc::c_int>
        {
            //do {
            //    ret = ioctl(fd, request, arg);
            //} while (ret == -1 && (errno == EINTR || errno == EAGAIN));
            loop {
                let ret = convert_ioctl_res!(
                    nix::libc::ioctl(
                        fd, 
                        request_code_readwrite!(
                            $ioty, $nr, 
                            ::std::mem::size_of::<$ty>()
                        ) as nix::sys::ioctl::ioctl_num_type, 
                        data
                    )
                );
                /*println!("{}\n", request_code_readwrite!(
                            $ioty, $nr, 
                            ::std::mem::size_of::<$ty>()
                        ) as nix::sys::ioctl::ioctl_num_type);*/
                if let Err(err) = ret {
                    if err == nix::Error::Sys(nix::errno::Errno::EINTR) || 
                        err == nix::Error::Sys(nix::errno::Errno::EAGAIN) {
                        continue;
                    }
                }
                return ret;
            }
        }
    )
}

#[macro_export]
macro_rules! xioctl_write_ptr {
    ($(#[$attr:meta])* $name:ident, $ioty:expr, $nr:expr, $ty:ty) => (
        $(#[$attr])*
        pub unsafe fn $name(fd: nix::libc::c_int, data: *const $ty) -> 
            nix::Result<nix::libc::c_int>
        {
            loop {
                let ret = convert_ioctl_res!(
                    nix::libc::ioctl(
                        fd,
                        request_code_write!(
                            $ioty, $nr, 
                            ::std::mem::size_of::<$ty>()
                        ) as nix::sys::ioctl::ioctl_num_type,
                        data
                    )
                );
                if let Err(err) = ret {
                    if err == nix::Error::Sys(nix::errno::Errno::EINTR) || 
                        err == nix::Error::Sys(nix::errno::Errno::EAGAIN) {
                        continue;
                    }
                }
                return ret;
            }
        }
    )
}
