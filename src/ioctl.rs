extern crate libc;

use std::os::raw::{c_int, c_ulong};
use std::ffi::c_void;

use self::libc::{EAGAIN, EINTR};

#[cfg(any(target_os = "android", target_env = "musl"))]
pub type num_type = c_int;
#[cfg(not(any(target_os = "android", target_env = "musl")))]
pub type num_type = c_ulong;
/// The datatype used for the 3rd argument
pub type param_type = c_ulong;

#[cfg(any(target_arch = "x86",
          target_arch = "arm",
          target_arch = "s390x",
          target_arch = "x86_64",
          target_arch = "aarch64",
          target_arch = "riscv32",
          target_arch = "riscv64"))]
mod consts {
    use ioctl::num_type;
    pub const NONE: num_type = 0x00000000;
    pub const READ: num_type = 0x80000000;
    pub const WRITE: num_type = 0x40000000;
}
#[cfg(any(target_arch = "mips", 
         target_arch = "mips64", 
         target_arch = "powerpc",
         target_arch = "powerpc64",
         target_arch = "sparc64"))]
mod consts {
    use ioctl::num_type;
    pub const NONE: num_type = 0x20000000;
    pub const READ: num_type = 0x40000000;
    pub const WRITE: num_type = 0x80000000;
}

const RW: num_type = 0xC0000000;

use self::consts::*;

// x86
// 1 1   1 1 1 1 1 1 1 1 1 1 1 1 1 1   1 1 1 1 1 1 1 1   1 1 1 1 1 1 1 1
// DIR               SIZE                    TYPE              NR

// MIPS
// 1 1 1   1 1 1 1 1 1 1 1 1 1 1 1 1   1 1 1 1 1 1 1 1   1 1 1 1 1 1 1 1
//  DIR              SIZE                   TYPE               NR

pub fn ioctl_none(fd: c_int, tp: num_type, nr: num_type) -> c_int 
{
    let req = 0x00000000 | ((tp & 0xFF) << 8) | (nr & 0xFF);
    let mut ret = EAGAIN;
    while ret == EINTR || ret == EAGAIN {
        ret = unsafe { libc::ioctl(fd, req) };
        continue;
    }
    return ret;
}

fn ioctl_r_(fd: c_int, sz: num_type, 
            tp: num_type, nr: num_type, 
            data: *mut c_void) -> c_int 
{
    let req = 0x80000000 | ((sz & 0x3FFF) << 16) |
            ((tp & 0xFF) << 8) | (nr & 0xFF);
    let mut ret = EAGAIN;
    while ret == EINTR || ret == EAGAIN {
        ret = unsafe { libc::ioctl(fd, req, data) };
        continue;
    }
    return ret;
}

fn ioctl_w_(fd: c_int, sz: num_type, 
            tp: num_type, nr: num_type, 
            data: *const c_void) -> c_int 
{
    let req = 0x40000000 | ((sz & 0x3FFF) << 16) |
            ((tp & 0xFF) << 8) | (nr & 0xFF);
    let mut ret = EAGAIN;
    while ret == EINTR || ret == EAGAIN {
        ret = unsafe { libc::ioctl(fd, req, data) };
        continue;
    }
    return ret;
}

fn ioctl_rw_(fd: c_int, sz: num_type, 
             tp: num_type, nr: num_type, 
             data: *mut c_void) -> c_int 
{
    let req = 0xC0000000 | ((sz & 0x3FFF) << 16) |
            ((tp & 0xFF) << 8) | (nr & 0xFF);
    let mut ret = EAGAIN;
    while ret == EINTR || ret == EAGAIN {
        ret = unsafe { libc::ioctl(fd, req, data) };
        continue;
    }
    return ret;
}

pub fn ioctl_r<T: Sized>(
            fd: c_int, tp: num_type,
            nr: num_type, data: &mut T) -> c_int
{
    let sz = std::mem::size_of::<T>() as num_type;
    ioctl_r_(fd, sz, tp, nr, data as *mut _ as *mut c_void)
}

pub fn ioctl_w<T: Sized>(
            fd: c_int, tp: num_type,
            nr: num_type, data: &T) -> c_int
{
    let sz = std::mem::size_of::<T>() as num_type;
    ioctl_w_(fd, sz, tp, nr, data as *const _ as *const c_void)
}


pub fn ioctl_rw<T: Sized>(
            fd: c_int, tp: num_type,
            nr: num_type, data: &mut T) -> c_int
{
    let sz = std::mem::size_of::<T>() as num_type;
    ioctl_rw_(fd, sz, tp, nr, data as *mut _ as *mut c_void)
}
