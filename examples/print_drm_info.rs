extern crate drm;

use drm::Dev;

fn main()
{
    let dev = Dev::open("/dev/dri/card0");
    let ver = dev.get_version();
    println!("Version {}.{}.{}", ver.major(), ver.minor(), ver.patchlevel());
    println!("Name: {:?}", ver.name());
    println!("Date: {:?}", ver.date());
    println!("Desc: {:?}", ver.desc());
    println!("token: {:?}", dev.get_token());
    println!("DRM_CAP_TIMESTAMP_MONOTONIC:  {:?}", dev.capTimestampMonotonic());
    println!("DRM_CAP_CURSOR_WIDTH:  {:?}", dev.capCursorWidth());
    println!("DRM_CAP_CURSOR_HEIGHT:  {:?}", dev.capCursorHeight());
    //set_property(d.as_raw_fd(), 51, 2, DRM_MODE_DPMS_STANDBY as u64);
    //loop {
    //}
    let rsrcs = dev.resources();
    let conns = rsrcs.connectors();
    for c in conns {
        println!("{:#?}", c);
        let props = c.properties();
        for p in props {
            println!("{:#?}", p);
        }
    }
}
