extern crate drm;

use drm::{Dev, Control};
use drm::{DRM_MODE_DPMS_STANDBY};

fn main()
{
    let dev = Dev::open("/dev/dri/card0");
    let rsrcs = dev.resources();
    let mut conns = rsrcs.connectors();
    let found_conn = conns.find(|ref x| x.is_connected()).unwrap();
    let mut props = found_conn.properties();
    let found_prop = props.find(|ref x| x.name().contains("DPMS")).unwrap();
        
    let id = found_conn.id();
    let prop_id = found_prop.id();
    
    println!("{:#?}  {:#?}  {:#?}", id, prop_id, DRM_MODE_DPMS_STANDBY);
    
    let cntrl = Control::new(&dev);
    let c = cntrl.get_connector(id);
    c.set_property(prop_id, DRM_MODE_DPMS_STANDBY.into());
    
    loop {
    }
}
